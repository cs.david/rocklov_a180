require "mongo"

Mongo::Logger.logger = Logger.new("./logs/mongo.log")

class MongoDB
  attr_accessor :client, :users, :equipos

  def initialize
    # client = Mongo::Client.new("mongodb://rocklov-db:27017/rocklov")
    # utiliza o valor de mongo definido no arquivo local.yml
    @client = Mongo::Client.new("mongodb://rocklov-db:27017/rocklov")
    @users = client[:users]
    @equipos = client[:equipos]
  end

  def drop_danger
    client.database.drop
  end

  def insert_users(docs)
    @users.insert_many(docs)
  end

  def remove_user(email)
    # realiza uma chamada no Módulo do Mongo,
    # que chama a classe Client
    # ativando uma nova execução passando a string de conexão

    # users = client[:users]
    @users.delete_many({ email: email })
  end

  def get_user(email)
    # users = client[:users]
    user = @users.find({ email: email }).first
    return user[:_id]
  end

  def remove_equipo(name, user_id)
    # converte a string para um object_id que o mongo entenda
    obj_id = BSON::ObjectId.from_string(user_id)
    @equipos.delete_many({ name: name, user: obj_id })
  end

  def get_mongo_id
    # cria um objectId mongo aleatório
    return BSON::ObjectId.new
  end
end
