#  DRY = Don't Repeat Yourself => Não se repita
# Quando falamos de desenvolvimento, deve se evitar a repetição

# Quando o assunto for TESTE, é mais importante deixar o teste claro na implementação

# describe cria uma suíte
describe "POST /sessions" do
  context "login com sucesso" do

    # executo o gancho como pré condição para os its de validação
    # dessa forma os expects ficam separados
    # o parâmetro :all no before indica que, para todas as minhas especificações (it's),
    # o gancho será executado apenas uma vez
    before(:all) do
      # armazena um hash com os valores que serão enviado para a api
      payload = { email: "betao@hotmail.com", password: "pwd123" }

      @result = Sessions.new.login(payload)
    end

    it "valida status code" do
      # realiza uma asserção do status code da requisição feita acima
      expect(@result.code).to eql 200
    end

    it "valida id do usuário" do
      # com o hash pode ser recuperado o valor da chave afim de realizar validações
      expect(@result.parsed_response["_id"].length).to eql 24
      # expect(@result.parsed_response["email"]).to eql "betao@hotmail.com"

      # necessário usar o parsed_response, para transformar o retorno da api em hash
      # puts @result.parsed_response["_id"]
      # puts @result.parsed_response.class
    end
  end

  #   #    array de hashes com a massa de cada teste
  #   examples = [
  #     {
  #       title: "senha incorreta",
  #       payload: { email: "betao@yahoo.com", password: "123456" },
  #       code: 401,
  #       error: "Unauthorized",
  #     },
  #     {
  #       title: "usuario nao existe",
  #       payload: { email: "404@yahoo.com", password: "pwd123" },
  #       code: 401,
  #       error: "Unauthorized",
  #     },
  #     {
  #       title: "email em branco",
  #       payload: { email: "", password: "pwd123" },
  #       code: 412,
  #       error: "required email",
  #     },
  #     {
  #       title: "sem o campo email",
  #       payload: { password: "pwd123" },
  #       code: 412,
  #       error: "required email",
  #     },
  #     {
  #       title: "senha em branco",
  #       payload: { email: "betao@yahoo.com", password: "" },
  #       code: 412,
  #       error: "required password",
  #     },
  #     {
  #       title: "sem o campo senha",
  #       payload: { email: "betao@yahoo.com" },
  #       code: 412,
  #       error: "required password",
  #     },
  #   ]

  # carrega a massa de um arquivo yml e transforma cada nome em simbolos, utilizando o parâmetro symbolize_names: true
  examples = Helpers::get_fixture("login")

  # através de um foreach, executo cada um dos cenários, e a cada rodada trocando a massa que está disponível
  # no array de hashes
  examples.each do |e|
    context "#{e[:title]}" do
      before(:all) do
        @result = Sessions.new.login(e[:payload])
      end

      it "valida status code #{e[:code]}" do
        # senha invalida retorna 401
        expect(@result.code).to eql e[:code]
      end

      it "valida mensagem de erro" do
        expect(@result.parsed_response["error"]).to eql e[:error]
      end
    end
  end
end
