describe "POST /signup" do
  context "novo usuario" do
    before(:all) do
      payload = { name: "Pitty", email: "pitty@bol.com.br", password: "pwd123" }
      MongoDB.new.remove_user(payload[:email])
      @result = Signup.new.create(payload)
    end

    it "valida status code" do
      # realiza uma asserção do status code da requisição feita acima
      expect(@result.code).to eql 200
    end

    it "valida id do usuário" do
      # com o hash pode ser recuperado o valor da chave afim de realizar validações
      expect(@result.parsed_response["_id"].length).to eql 24
    end
  end

  examples = [
    {
      title: "usuario ja existe",
      payload: { name: "Joao da Silva", email: "joao@ig.com.br", password: "pwd123" },
      code: 409,
      error: "Email already exists :(",
    },
    {
      title: "nome em branco",
      payload: { name: "", email: "joao@ig.com.br", password: "pwd123" },
      code: 412,
      error: "required name",
    },
    {
      title: "sem o campo nome",
      payload: { email: "joao@ig.com.br", password: "pwd123" },
      code: 412,
      error: "required name",
    },
    {
      title: "email em branco",
      payload: { name: "Joao da Silva", email: "", password: "pwd123" },
      code: 412,
      error: "required email",
    },
    {
      title: "sem o campo email",
      payload: { name: "Joao da Silva", password: "pwd123" },
      code: 412,
      error: "required email",
    },
    {
      title: "senha em branco",
      payload: { name: "Joao da Silva", email: "joao@ig.com.br", password: "" },
      code: 412,
      error: "required password",
    },
    {
      title: "sem o campo senha",
      payload: { name: "Joao da Silva", email: "joao@ig.com.br" },
      code: 412,
      error: "required password",
    },
  ]

  examples.each do |e|
    context "#{e[:title]}" do
      before(:all) do
        payload = e[:payload]
        MongoDB.new.remove_user(payload[:email])

        Signup.new.create(payload)
        @result = Signup.new.create(payload)
      end

      it "deve retornar #{e[:code]}" do
        expect(@result.code).to eql e[:code]
      end

      it "deve retornar mensagem" do
        expect(@result.parsed_response["error"]).to eql e[:error]
      end
    end
  end
end
