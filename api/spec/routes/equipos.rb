require_relative "base_api"

class Equipos < BaseApi
  def create(payload, user_id)

    # monta um requisição do tipo POST
    # self torna a classe Sessions o próprio HTTParty, fazendo com que tenha acessos ao objeto da própria classe
    # self é semelhante ao this de outras linguagens
    return self.class.post(
             "/equipos", # informa o endpoint
             body: payload,
             headers: {
               "user_id": user_id,
             },
           )
  end

  def bookign(equipo_id, user_locator_id)
    return self.class.post(
             "/equipos/#{equipo_id}/bookings",
             body: { date: Time.now.strftime("%d/%m/%Y") }.to_json,
             headers: {
               "user_id": user_locator_id,
             },
           )
  end

  def find_by_id(equipo_id, user_id)
    return self.class.get(
             "/equipos/#{equipo_id}",
             headers: {
               "user_id": user_id,
             },
           )
  end

  def remove_by_id(equipo_id, user_id)
    return self.class.delete(
             "/equipos/#{equipo_id}",
             headers: {
               "user_id": user_id,
             },
           )
  end

  def list(user_id)
    return self.class.get(
             "/equipos/",
             headers: {
               "user_id": user_id,
             },
           )
  end
end
