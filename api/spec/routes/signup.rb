require_relative "base_api"

class Signup < BaseApi
  def create(payload)

    # monta um requisição do tipo POST
    # self torna a classe Sessions o próprio HTTParty, fazendo com que tenha acessos ao objeto da própria classe
    # self é semelhante ao this de outras linguagens
    return self.class.post(
             "/signup", # informa o endpoint
             body: payload.to_json, # converte o hash para json
             headers: {
               "Content-type": "application/json", # informa no cabeçalho o tipo de informção que será enviada
             },
           )
  end
end
