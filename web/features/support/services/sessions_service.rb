require_relative "base_service"

class SessionsService < BaseService
  def get_user_id(email, password)
    payload = { email: email, password: password }
    result = self.class.post(
      "/sessions", # informa o endpoint
      body: payload.to_json, # converte o hash para json
      headers: {
        "Content-type": "application/json", # informa no cabeçalho o tipo de informção que será enviada
      },
    )
    return result.parsed_response["_id"]
  end
end
