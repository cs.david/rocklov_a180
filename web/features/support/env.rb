require "allure-cucumber"
require "capybara"
require "capybara/cucumber"
require "faker"

# ENV["Variavel de Ambiente"]
# Recupera o valor do CONFIG do arquivo cucumber.yml
CONFIG = YAML.load_file(File.join(Dir.pwd, "features/support/config/#{ENV["CONFIG"]}"))

# cria a constante BROWSER e pega o valor da variável de ambiente BROWSER
BROWSER = ENV["BROWSER"]

case ENV["BROWSER"]
when "firefox"
  @driver = :selenium
when "fire_headless"
  @driver = :selenium_headless
when "chrome"
  @driver = :selenium_chrome
when "chrome_headless"
  Capybara.register_driver :selenium_chrome_headless do |app|
    Capybara::Selenium::Driver.load_selenium
    browser_options = ::Selenium::WebDriver::Chrome::Options.new.tap do |opts|
      opts.args << "--headless"
      opts.args << "--disable-gpu"
      opts.args << "--disable-site-isolation-trials"
      opts.args << "--no-sandbox"
      opts.args << "--disable-dev-shm-usage"
    end
    Capybara::Selenium::Driver.new(app, browser: :chrome, options: browser_options)
  end

  @driver = :selenium_chrome_headless
else
  raise "Navegador inválido a variavel @driver está vazia:("
end

Capybara.configure do |config|
  config.default_driver = @driver
  # utiliza o url definido no arquivo local.yml
  config.app_host = CONFIG["url"]
  config.default_max_wait_time = 10
end

AllureCucumber.configure do |config|
  # define o caminho onde será salvo o report
  config.results_directory = "/logs"

  # indica que a cada nova execução a pasta log será limpada
  config.clean_results_directory = true
end
