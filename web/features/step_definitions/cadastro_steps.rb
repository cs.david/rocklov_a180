Dado("que acesso a página de cadastro") do
  #abre o navegador configurado no env.rb e navega para o endereço da string
  # visit "/signup"
  @signup_page.open
end

Quando("submeto o seguinte formulário de cadastro:") do |table|
  user = table.hashes.first
  MongoDB.new.remove_user(user[:email])
  @signup_page.create(user)
end

################################ Métodos comentados para auxilio ####################################
# Quando("submeto o meu cadastro completo") do
#   MongoDB.new.remove_user("david@gmail.com")

#   # seta os valores nos campos nome, email e senha
#   # para localizar elementos por id, deve-se usar #
#   find("#fullName").set "David Costa"

#   # utilizado o Faker para geração de emails dinâmicos, afim de garantir que
#   # cada execução tenha massa válida
#   #   find("#email").set Faker::Internet.free_email
#   find("#email").set "david@gmail.com"
#   find("#password").set "pwd123"

#   # clica no botão Cadastrar, neste caso foi passado o texto presente no botão
#   click_button "Cadastrar"
# end
