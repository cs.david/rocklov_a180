Dado("que acesso a página principal") do
  @login_page = LoginPage.new
  @login_page.open_page
end

Quando("submeto minhas credenciais com {string} e {string}") do |email, password|
  # o mapeamento dos elementos da página de login estão na classe LoginPage em pages/login_page.rb
  @login_page.with(email, password)
end
